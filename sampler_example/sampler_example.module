<?php

/**
 * @file
 * An example module that demonstrates how to use Sampler API.
 */

/**
 * Implements hook_menu().
 */
function sampler_example_menu() {
  $items = array();
  // Callback for testing purposes.
  $items['admin/sampler-example-test/%/%'] = array(
    'title' => 'Testing function for Sampler API module',
    'page callback' => 'sampler_example_test',
    'page arguments' => array(2, 3),
    'access callback' => 'user_access',
    'access arguments' => array('administer site configuration'),
    'type' => MENU_CALLBACK,
  );
  return $items;
}

/**
 * Implements hook_ctools_plugin_directory().
 *
 * This is a required hook, as CTools must be able to find the plugins to load
 * them.
 *
 * For sampler, valid plugin types are 'metric', 'adjustment',
 * 'method', 'storage'.
 */
function sampler_example_ctools_plugin_directory($module, $plugin) {
  if ($module == 'sampler') {
    switch ($plugin) {
      case 'metric':
        return 'metrics';
    }
  }
}

/**
 * Testing function for triggering sampling from a URL.
 *
 * You can specify sample options as query arguments, as well as the option
 * 'save' to save the returned computed values.
 * to storage.
 *
 * @param $module
 *   The module advertising the metric.
 * @param $metric
 *   The metric being tracked.
 * @return
 *   An HTML table containing the requested metrics info, and a spiffy Google
 *   Chart API sparkline.
 */
function sampler_example_test($module, $metric) {
  $build = array();

  // Required setup.
  sampler_load_sampler();
  $object = new Sampler();

  // Add GET params to the metric options.  It's a bit ugly, but provides an
  // easy way to test option overrides.
  if (isset($_GET) && is_array($_GET)) {
    // Convert options to proper type/format.
    foreach ($_GET as $key => $value) {
      if ($key == 'object_ids') {
        $value = explode(',', $value);
      }
      elseif (is_numeric($value)) {
        $value = (int) $value;
      }
      $options[$key] = $value;
    }
  }

  // Try to get a new sampler.
  if ($sampler = $object->newSampler($module, $metric, $options)) {

    // Compute and optionally save the metrics.
    $samples = $sampler->computeSamples();
    if (isset($options['save'])) {
      $sampler->saveSamples($samples);
    }

    // Grab the number of samples taken from the sample object.
    $sample_points = $sampler->samplesComputed;

    // Display simple tables of results, one table per timestamp, and calculate
    // and throw in a Google chart using the Google Chart API for fun.
    $chart_values = array();
    $header = array(
      t('Object ID'),
      t('Value'),
    );
    $max = array();
    $min = array();

    foreach ($samples as $sample) {
      $rows = array();
      $value_total = array();
      $count = 0;
      $avg_value = array();
      foreach ($sample->values as $id => $values) {
        $rows[] = array(
          check_plain($id),
          check_plain(implode(', ', $values)),
        );
        $count++;
        // Support multiple values.
        foreach ($values as $key => $value) {
          if (!isset($value_total[$key])) {
            $value_total[$key] = 0;
          }
          $value_total[$key] = $value_total[$key] + intval($values[$key]);
        }
      }
      // Use the keys of the returned values to do our min/max/avg stuff
      // per value.
      if ($count > 0) {
        foreach ($value_total as $key => $value) {
          $avg_value[$key] = $value_total[$key] / $count;
          $chart_values[$key][] = $avg_value[$key];
          if (!isset($max[$key]) || $avg_value[$key] > $max[$key]) {
            $max[$key] = $avg_value[$key];
          }
          if (!isset($min[$key]) || $avg_value[$key] < $min[$key]) {
            $min[$key] = $avg_value[$key];
          }
          if (!isset($chart_max) || $avg_value[$key] > $chart_max) {
            $chart_max = $avg_value[$key];
          }
          if (!isset($chart_min) || $avg_value[$key] < $chart_min) {
            $chart_min = $avg_value[$key];
          }
        }
      }

      $build['metric_detail_table'][] = array(
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
        '#caption' => check_plain(format_date($sample->timestamp)),
      );
    }

    if (!empty($build)) {
      $min_max = '';
      if ($sampler->methodPlugin == 'periodic') {
        $min_max = "
  <dt>". t("Max:") ."</dt>
    <dd>". check_plain(implode(', ', $max)) ."</dd>
  <dt>". t("Min:") ."</dt>
    <dd>". check_plain(implode(', ', $min)) ."</dd>
";
      }
      $summary_list = "
<dl>
$min_max
  <dt>". t("Description:") ."</dt>
    <dd>". check_plain(implode(' ', $sampler->pluginOutput)) ."</dd>
  <dt>". t("Sample points:") ."</dt>
    <dd>". check_plain($sample_points) ."</dd>
</dl>
";
      $metric_summary = array(
        'metric_summary' => array(
          '#markup' => $summary_list,
        ),
      );
      $build = array_merge($metric_summary, $build);
    }

    drupal_set_title(t('Module: %module, Metric: %metric', array('%module' => $module, '%metric' => $sampler->title,)), PASS_THROUGH);

    // Don't display a chart for a single sample, or if the sampling method is
    // not periodic.
    if (empty($options['single_sample']) && !empty($chart_values) && $sampler->methodPlugin == 'periodic') {
      $value_list = array();
      $colors = array(
        '0000FF',
        'FF0000',
        '00FF00',
        '000000',
        '49188F',
        'AA0033',
        '80C65A',
        '3399CC',
      );
      $chart_colors = array();
      $legend = array();
      foreach ($values as $key => $value) {
        $value_list[$key] = implode(',', $chart_values[$key]);
        $legend[] = $key;
        $chart_colors[] = current($colors);
        next($colors);
        if (!current($colors)) {
          reset($colors);
        }
      }
      $final_value_list = implode('|', $value_list);
      $final_chart_colors = implode(',', $chart_colors);
      $final_legend = urlencode(implode('|', $legend));
      $google_chart = array(
        'google_chart' => array(
          '#prefix' => '<p>',
          '#markup' => "<img alt=\"Google Chart API sparkline\" src=\"http://chart.apis.google.com/chart?cht=lc&chs=1000x265&chd=t:$final_value_list&chds=$chart_min,$chart_max&chco=$final_chart_colors&chls=2,1,0&chdl=$final_legend\">",
          '#suffix' => '</p>',
        ),
      );
      $build = array_merge($google_chart, $build);
    }
  }
  else {
    drupal_set_message(t("Could not load sampler for module %module, metric %metric", array('%module' => $module, '%metric' => $metric)), 'error');
  }

  if (empty($build)) {
    $build['no_content'] = array(
      '#prefix' => '<p>',
      '#markup' => t('No values to display'),
      '#suffix' => '</p>',
    );
  }
  return $build;
}

