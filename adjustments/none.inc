<?php


/**
 * @file
 * Dummy adjustment plugin.
 *
 * This plugin is just a stub to allow for not having any adjustments to
 * samples.
 */

$plugin = array(
  'handler' => array(
    'class' => 'SamplerAdjustmentHandlerNone',
  ),
);
