<?php

/**
 * @file
 * Drupal table storage plugin for multiple metrics per table.
 */

$plugin = array(
  'handler' => array(
    'class' => 'SamplerStorageHandlerDrupalDatabaseTable',
  ),
);

